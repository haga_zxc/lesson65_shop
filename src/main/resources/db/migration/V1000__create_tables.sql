use `online_shop`;

create table `product_type`
(
    `id`   INT auto_increment NOT NULL,
    `name` varchar(128)       NOT NULL,
    `image` varchar(250)       NOT NULL,
    PRIMARY KEY (`id`)
);
create table `products`
(
    `id`              INT auto_increment NOT NULL,
    `name`            varchar(128)       NOT NULL,
    `image`           varchar(250)       NOT NULL,
    `description`     varchar(500)       NOT NULL,
    `qty`             INTEGER            NOT NULL,
    `price`           double             not null,
    `product_type_id` int                not null,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_product_type`
        FOREIGN KEY (`product_type_id`) REFERENCES `product_type` (`id`)
);

CREATE TABLE `customers` (
     `id` int auto_increment NOT NULL,
     `email` varchar(128) NOT NULL,
     `password` varchar(128) NOT NULL,
     `fullname` varchar(128) NOT NULL default ' ',
     `enabled` boolean NOT NULL default true,
     `role` varchar(16) NOT NULL default 'USER',
     PRIMARY KEY (`id`),
     UNIQUE INDEX `email_unique` (`email` ASC)
);

CREATE TABLE `cart`(
    `id`           INT          NOT NULL AUTO_INCREMENT,
    `customers_id` INT(11)      NULL,
    `session`      VARCHAR(128) NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_cart`
        FOREIGN KEY (`customers_id`) REFERENCES `customers` (`id`)
);

CREATE TABLE `products_and_cart`(
    `id`           INT          NOT NULL AUTO_INCREMENT,
    `products_id` INT(11) null,
    `cart_id`     INT(11) null,
    `qty`         INT     NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `fk_products_and_cart_products1`
        FOREIGN KEY (`products_id`) REFERENCES `products` (`id`),
    CONSTRAINT `fk_products_and_cart_cart1`
        FOREIGN KEY (`cart_id`) REFERENCES `cart` (`id`)
);
CREATE TABLE `resets`(
   `id`           INT          NOT NULL AUTO_INCREMENT,
   `customer_id` INT(11)      NULL,
   `token`      VARCHAR(128) NOT NULL,
   PRIMARY KEY (`id`),
   CONSTRAINT `fk_resets`
       FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`)
);
CREATE TABLE `reviews`(
     `id`           INT          NOT NULL AUTO_INCREMENT,
     `customer_id` INT(11)      NULL,
     `product_id` INT(11)      NULL,
     `text`      VARCHAR(400) NOT NULL,
     `rating` INT(5)      not null ,
     PRIMARY KEY (`id`),
     CONSTRAINT `fk_reviews`
         FOREIGN KEY (`customer_id`) REFERENCES `customers` (`id`),
     CONSTRAINT `fk_reviews2`
             FOREIGN KEY (`product_id`) REFERENCES `products` (`id`)
);