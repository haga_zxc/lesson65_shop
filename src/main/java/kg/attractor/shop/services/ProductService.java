package kg.attractor.shop.services;

import kg.attractor.shop.DTO.ProductAndCartDTO;
import kg.attractor.shop.DTO.ProductDTO;
import kg.attractor.shop.model.Cart;
import kg.attractor.shop.model.Customer;
import kg.attractor.shop.model.Product;
import kg.attractor.shop.model.Products_and_cart;
import kg.attractor.shop.repository.CartRepository;
import kg.attractor.shop.repository.CustomerRepository;
import kg.attractor.shop.repository.ProductRepository;
import kg.attractor.shop.repository.Products_and_cartRepository;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ProductService {
    private final ProductRepository pr;
    private final Products_and_cartRepository pacr;
    private final CartRepository cr;
    private final CustomerRepository custRep;

    public Page<ProductDTO> getProducts(int id_type, Pageable pageable){
        return pr.findAllByTypeId(id_type,pageable)
                .map(ProductDTO::from);
    }
    public Page<ProductDTO> getProductSearch(String text,Pageable pageable){
        return pr.findProductsByNameAndDescription(text, pageable)
                .map(ProductDTO::from);
    }
    public ProductDTO getProductById(int id){
        var product = pr.findProductById(id);
        ProductDTO p = ProductDTO.from(product);
        return p;
    }

    public Page<ProductAndCartDTO> getCartProducts(Authentication authentication,Pageable pageable){
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        var user = custRep.findByEmail(userDetails.getUsername());
        Cart cart = cr.findByCustomerId(user.getId());
        Page<Products_and_cart> productPage = pacr.findAllByCartId(cart.getId(), pageable);
        return productPage.map(ProductAndCartDTO::from);
    }

    public void addToCart(Authentication authentication, int id){
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        var product = pr.findProductById(id);
        var user = custRep.findByEmail(userDetails.getUsername());
        if(!cr.existsById(user.getId())) {
            var cart = Cart.builder()
                    .customer(user)
                    .session("gjiwoigwh")
                    .build();
            cr.save(cart);
        }
            var productAndCart = Products_and_cart
                    .builder()
                    .product(product)
                    .cart(cr.findByCustomerId(user.getId()))
                    .qty(1).build();
        pacr.save(productAndCart);
    }
    public void delete(int id){
        var productCart = pacr.findById(id);
        pacr.delete(productCart.get());

    }
    public Page<ProductAndCartDTO> makeNewOrder(int id,Pageable pageable){
         return pacr.findAllByCartId(id,pageable).map(ProductAndCartDTO::from);

    }

}