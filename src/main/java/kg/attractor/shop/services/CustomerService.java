package kg.attractor.shop.services;

import kg.attractor.shop.DTO.CustomerResponseDTO;
import kg.attractor.shop.exception.CustomerAlreadyRegisteredException;
import kg.attractor.shop.exception.CustomerNotFoundException;
import kg.attractor.shop.model.Customer;
import kg.attractor.shop.model.CustomerRegisterForm;
import kg.attractor.shop.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class CustomerService {

    private final CustomerRepository repository;
    private final PasswordEncoder encoder;

    public CustomerResponseDTO register(CustomerRegisterForm form) {
        if (repository.existsByEmail(form.getEmail())) {
            throw new CustomerAlreadyRegisteredException();
        }

        var user = Customer.builder()
                .email(form.getEmail())
                .fullname(form.getName())
                .password(encoder.encode(form.getPassword()))
                .build();

        repository.save(user);

        return CustomerResponseDTO.from(user);
    }
}
