package kg.attractor.shop.services;

import kg.attractor.shop.DTO.ReviewDTO;
import kg.attractor.shop.model.Review;
import kg.attractor.shop.repository.*;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ReviewService {
    private final ProductRepository pr;
    private final CustomerRepository cr;
    private final ReviewRepository rr;

    public void makeReview(Authentication authentication, int id,String text, int rating){
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        var user = cr.findByEmail(userDetails.getUsername());
        var review = Review.builder()
                .customer(user)
                .product(pr.findProductById(id))
                .rating(rating)
                .text(text)
                .build();
        rr.save(review);
    }
    public Page<ReviewDTO> getReviews(int id, Pageable pageable){
        var reviews = rr.findAllByProductId(id,pageable);
        return reviews
                .map(ReviewDTO::from);
    }
}
