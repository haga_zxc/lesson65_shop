package kg.attractor.shop.controller;

import kg.attractor.shop.model.Customer;
import kg.attractor.shop.model.CustomerRegisterForm;
import kg.attractor.shop.repository.CustomerRepository;
import kg.attractor.shop.repository.ResetRepository;
import kg.attractor.shop.services.CustomerService;
import kg.attractor.shop.model.PasswordResetToken;
import kg.attractor.shop.services.ProductService;
import kg.attractor.shop.services.PropertiesService;
import kg.attractor.shop.services.ReviewService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.UUID;

@Controller
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@RequestMapping("/main")
public class MainController {
    private final CustomerService customerService;
    private final ProductService productService;
    private final PropertiesService propertiesService;
    private final CustomerRepository customerRepository;
    private final ResetRepository resetRepo;
    private final ReviewService reviewService;

    @GetMapping
    public String mainPage() {
        return "main";
    }

    @GetMapping("/registration")
    public String pageRegisterCustomer(Model model) {
        if (!model.containsAttribute("dto")) {
            model.addAttribute("dto", new CustomerRegisterForm());
        }
        return "registration";
    }

    @PostMapping("/registration")
    public String registerPage(@Valid CustomerRegisterForm customerRequestDto,
                               BindingResult validationResult,
                               RedirectAttributes attributes) {
        attributes.addFlashAttribute("dto", customerRequestDto);

        if (validationResult.hasFieldErrors()) {
            attributes.addFlashAttribute("errors", validationResult.getFieldErrors());
            return "redirect:/main/registration";
        }

        customerService.register(customerRequestDto);
        return "redirect:/main/login";
    }

    @GetMapping("/login")
    public String loginPage(@RequestParam(required = false, defaultValue = "false") Boolean error, Model model) {
        model.addAttribute("error", error);
        return "login";
    }

    private static <T> void constructPageable(Page<T> list, int pageSize, Model model, String uri) {
        if (list.hasNext()) {
            model.addAttribute("nextPageLink", constructPageUri(uri, list.nextPageable().getPageNumber(), list.nextPageable().getPageSize()));
        }

        if (list.hasPrevious()) {
            model.addAttribute("prevPageLink", constructPageUri(uri, list.previousPageable().getPageNumber(), list.previousPageable().getPageSize()));
        }

        model.addAttribute("hasNext", list.hasNext());
        model.addAttribute("hasPrev", list.hasPrevious());
        model.addAttribute("items", list.getContent());
        model.addAttribute("defaultPageSize", pageSize);
    }
    private static String constructPageUri(String uri, int page, int size) {
        return String.format("%s?page=%s&size=%s", uri, page, size);
    }
    @GetMapping("/{id}")
    public String index(Model model, Pageable pageable, HttpServletRequest uriBuilder,@PathVariable("id")int id) {
        var products = productService.getProducts(id, pageable);
        var uri = uriBuilder.getRequestURI();
        constructPageable(products, propertiesService.getDefaultPageSize(), model, uri);

        return "products";
    }
    @GetMapping("/product/{id}")
    public String product(Model model,@PathVariable("id")int id,Pageable pageable){
        var product = productService.getProductById(id);
        model.addAttribute("product",product);
        model.addAttribute("reviews",reviewService.getReviews(id,pageable).getContent());
        return "single_product";
    }

    @GetMapping("/search/{search}")
    public String search(@PathVariable("search") String search,Model model, Pageable pageable, HttpServletRequest uriBuilder){
        var products = productService.getProductSearch(search, pageable);
        var uri = uriBuilder.getRequestURI();
        constructPageable(products, propertiesService.getDefaultPageSize(), model, uri);
        return "products";
    }

    @GetMapping("/logout")
    public String invalidate(HttpSession session) {
        if (session != null) {
            session.invalidate();
        }

        return "redirect:/main";
    }
    @GetMapping("/cart")
    public String getCart(Authentication authentication, Model model, HttpServletRequest uriBuilder,Pageable pageable) {
            var products = productService.getCartProducts(authentication,pageable);
            var uri = uriBuilder.getRequestURI();
            constructPageable(products,propertiesService.getDefaultPageSize(),model,uri);
            return "cart";
        }

        @PostMapping("/cart/add")
    public String addToCart(Authentication authentication,@RequestParam("id") int id){
            productService.addToCart(authentication,id);
        return "redirect:/main/cart";
        }

        @PostMapping("/cart/delete")
    public String deleteFromCart( @RequestParam("product_id")int id){
            productService.delete(id);
        return "redirect:/main/cart";
        }

        @GetMapping("/makeOrder/{id}")
    public String makeOrder(Model model,Pageable pageable,@PathVariable("id")int id){
            model.addAttribute("products",productService.makeNewOrder(id,pageable).getContent());
            return "order";
        }

    @GetMapping("/forgot-password")
    public String pageForgotPassword() {
        return "forgot";
    }

    @PostMapping("/forgot-password")
    public String submitForgotPasswordPage(@RequestParam("email") String email,
                                           RedirectAttributes attributes) {

        if (!customerRepository.existsByEmail(email)) {
            attributes.addFlashAttribute("errorText", "Entered email does not exist!");
            return "redirect:/main";
        }

        PasswordResetToken pToken = PasswordResetToken.builder()
                .customer(customerRepository.findByEmail(email))
                .token(UUID.randomUUID().toString())
                .build();

        resetRepo.deleteAll();
        resetRepo.save(pToken);

        return "redirect:/main/forgot-success";
    }

    @GetMapping("/forgot-success")
    public String pageResetPassword() {
        return "forgot-success";
    }

    @PostMapping("/reset-password")
    public String submitResetPasswordPage(@RequestParam("token") String token,
                                          @RequestParam("newPassword") String newPassword,
                                          RedirectAttributes attributes) {

        if (!resetRepo.existsByToken(token)) {
            attributes.addFlashAttribute("errorText", "Entered email does not exist!");
            return "redirect:/main/reset-password";
        }

        PasswordResetToken pToken = resetRepo.findByToken(token).get();
        Customer customer = customerRepository.findById(pToken.getCustomer().getId()).get();
        customer.setPassword(new BCryptPasswordEncoder().encode(newPassword));

        customerRepository.save(customer);

        return "redirect:/main/login";
    }
    @PostMapping("/addReview")
    public String addReview(Authentication authentication,@RequestParam("id") int id,
                            @RequestParam("text")String text,
                            @RequestParam("rating")int rating) {
        reviewService.makeReview(authentication, id, text, rating);
        return "redirect:/main/product/" + id;
    }
    }
