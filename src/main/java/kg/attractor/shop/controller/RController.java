package kg.attractor.shop.controller;

import kg.attractor.shop.DTO.ProductDTO;
import kg.attractor.shop.services.ProductService;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class RController {
    private final ProductService productService;

    @GetMapping("/products/{id}")
    public List<ProductDTO> getProducts(@PathVariable("id")int id, Pageable pageable){
        return productService.getProducts(id,pageable).getContent();
    }

    @GetMapping("/search/{search}")
   public List<ProductDTO> search(@PathVariable("search") String search, Pageable pageable){
        return productService.getProductSearch(search,pageable).getContent();
    }
}
