package kg.attractor.shop.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Entity
@ToString
@Table(name = "products")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    private String name;
    @NotBlank
    @Size(min = 1, max = 250)
    @Column(length = 250)
    private String image;
    @NotBlank
    @Size(min = 1, max = 500)
    @Column(length = 500)
    private String description;
    @NotBlank
    @Column(length = 128)
    private Integer qty;
    @Column
    private double price;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_type_id")
    private ProductType type;

    @OneToMany(mappedBy = "product")
    List<Products_and_cart> products_and_carts;

}
