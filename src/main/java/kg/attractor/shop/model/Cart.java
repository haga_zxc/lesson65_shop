package kg.attractor.shop.model;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.List;


@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Entity
@ToString
@Table(name = "cart")
public class Cart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne
    @JoinColumn(name = "customers_id")
    private Customer customer;

    @NotBlank
    @Size(min = 1, max = 128)
    @Column(length = 128)
    private String session;

    @OneToMany(mappedBy = "cart")
    List<Products_and_cart> products_and_carts;

}
