package kg.attractor.shop.DTO;

import kg.attractor.shop.model.ProductType;
import lombok.*;

@Getter
@Setter
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
public class ProductTypeDTO {
    private Integer id;
    private String name;
    private String image;

    public static ProductTypeDTO from(ProductType productType) {
        return builder()
                .id(productType.getId())
                .name(productType.getName())
                .image(productType.getImage())
                .build();
    }
}
