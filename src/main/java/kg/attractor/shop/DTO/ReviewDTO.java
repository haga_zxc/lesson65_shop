package kg.attractor.shop.DTO;

import kg.attractor.shop.model.Review;
import lombok.*;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(access = AccessLevel.PACKAGE)
@ToString
public class ReviewDTO {
    private int id;
    private ProductDTO product;
    private int rating;
    private CustomerDTO customer;
    private String text;

    public static ReviewDTO from(Review review){
        return builder()
                .customer(CustomerDTO.from(review.getCustomer()))
                .id(review.getId())
                .product(ProductDTO.from(review.getProduct()))
                .rating(review.getRating())
                .text(review.getText())
                .build();
    }

}
