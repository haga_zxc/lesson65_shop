package kg.attractor.shop.DTO;

import kg.attractor.shop.model.Cart;
import lombok.*;

@Data
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@Builder(access = AccessLevel.PACKAGE)
@ToString
public class CartDTO {
    private int id;
    private CustomerDTO customer;
    private String session;

    public static CartDTO from(Cart cart){
        return builder()
                .customer(CustomerDTO.from(cart.getCustomer()))
                .id(cart.getId())
                .session(cart.getSession())
                .build();
    }
}
