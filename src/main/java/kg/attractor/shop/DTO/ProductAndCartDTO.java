package kg.attractor.shop.DTO;



import kg.attractor.shop.model.Products_and_cart;
import lombok.*;


@Getter
@Setter
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
public class ProductAndCartDTO {
    private int id;
    private ProductDTO product;
    private CartDTO cart;

    public static ProductAndCartDTO from(Products_and_cart products_and_cart) {
        return builder()
                .id(products_and_cart.getId())
                .product(ProductDTO.from(products_and_cart.getProduct()))
                .cart(CartDTO.from(products_and_cart.getCart()))
                .build();
    }
}
