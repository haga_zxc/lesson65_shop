package kg.attractor.shop.DTO;

import kg.attractor.shop.model.Product;
import lombok.*;

@Getter
@Setter
@Builder(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@ToString
public class ProductDTO {
    private Integer id;
    private String name;
    private String image;
    private String description;
    private Integer qty;
    private double price;
    private ProductTypeDTO type;

    public static ProductDTO from(Product product) {
        return builder()
                .id(product.getId())
                .name(product.getName())
                .image(product.getImage())
                .description(product.getDescription())
                .qty(product.getQty())
                .price(product.getPrice())
                .type(ProductTypeDTO.from(product.getType()))
                .build();
    }
}
