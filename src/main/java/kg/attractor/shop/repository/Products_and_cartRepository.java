package kg.attractor.shop.repository;

import kg.attractor.shop.model.Products_and_cart;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;


public interface Products_and_cartRepository extends JpaRepository<Products_and_cart, Integer> {
    Page<Products_and_cart> findAllByCartId(int id, Pageable pageable);


}
