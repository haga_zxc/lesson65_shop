package kg.attractor.shop.repository;

import kg.attractor.shop.model.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ProductRepository extends JpaRepository<Product, Integer> {
    Page<Product> findAllByTypeId(int id, Pageable pageable);

    @Query("SELECT p FROM Product p WHERE (p.description like concat(:name, '%')) or (p.description like concat('%',:name,'%')) or (p.description like concat('%', :name))or (p.name like concat(:name, '%')) or (p.name like concat('%',:name, '%') ) or (p.name like concat('%', :name)) ")
    Page<Product> findProductsByNameAndDescription(String name, Pageable pageable);
    Product findProductById(int id);

    Product findById(int id);
}

