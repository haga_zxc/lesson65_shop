package kg.attractor.shop.repository;

import kg.attractor.shop.model.Cart;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartRepository  extends JpaRepository<Cart, Integer> {
    Cart findByCustomerId(int id);
}
