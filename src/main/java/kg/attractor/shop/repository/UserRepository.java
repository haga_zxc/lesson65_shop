package kg.attractor.shop.repository;

import kg.attractor.shop.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<Customer,Integer> {
}
