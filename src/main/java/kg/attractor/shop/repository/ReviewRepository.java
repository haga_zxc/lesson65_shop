package kg.attractor.shop.repository;

import kg.attractor.shop.model.Review;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReviewRepository extends JpaRepository<Review,Integer> {
    Page<Review> findAllByProductId(int id, Pageable pageable);
}
