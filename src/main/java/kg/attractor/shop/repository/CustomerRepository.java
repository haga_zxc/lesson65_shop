package kg.attractor.shop.repository;

import kg.attractor.shop.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;


public interface CustomerRepository extends JpaRepository<Customer, Integer> {

    boolean existsByEmail(String email);
    Customer findByEmail(String email);

}
